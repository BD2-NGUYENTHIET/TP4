
set serveroutput on

create or replace procedure authentification
(
    l_idc client.idc%type,
    le_nom client.nom%type
)
is
    cursor c is
        SELECT * FROM client
            WHERE idc = l_idc
            AND nom = le_nom;
    le_client client%rowtype;
begin
    open c;
    fetch c into le_client;
    if c%found then
        dbms_output.put_line('Bienvenue ' || le_client.idc || ' ' || le_client.nom || ' ' || le_client.age || ' ' || le_client.avoir);
    else
        dbms_output.put_line('Vous n existez pas, dsl');
    end if;
end;
/

create or replace procedure consulter_informations(
    l_idc client.idc%type
)
is
    cursor c is
    select village.idv, ville, activite, prix, capacite
        from village, sejour
            where sejour.idc = l_idc
            and village.idv = sejour.idv;
    le_village village%rowtype;
begin
    for x in (
        select ids,idv,jour
            from sejour
            where sejour.idc = l_idc
    )
    loop
        dbms_output.put_line(x.ids || ' se passe dans ' || x.idv || ' le ' || x.jour);
    end loop;
    open c;
        fetch c into le_village;
        while c%found loop
            dbms_output.put_line(
                'Le village ' || le_village.idv || ' est dans la ville ' || le_village.ville
                || ' avec l activite ' || le_village.activite || ' avec le prix ' || le_village.prix 
                || ' avec la capacite ' || le_village.capacite
            );
            fetch c into le_village;
    end loop;
    close c;
    dbms_output.put_line('Fin');
end;
/

create or replace procedure traitement2(
    l_idc client.idc%type,
    la_ville village.ville%type,
    le_jour sejour.jour%type,
    l_idv out village.idv%type,
    l_ids out sejour.ids%type,
    l_activite out village.activite%type,
) 
is
    cursor c is 
        select * from village
            where village.ville = la_ville
            and village.prix = (
                select max(prix) from village
            );
    le_village village%rowtype;
begin
    open c;
    fetch c into le_village;
    if c%found then
        return (1,1,'quelque chose');
    else
        return (-1,-1,'néant');
    end if;
end;
/















